function CheckPorts
{
    Param([Parameter(Mandatory=$true)][string[]]$hostnames,
        [Parameter(Mandatory=$true)][int[]] $ports);

    foreach($hostname in $hostnames)
    {
        $hostExists = (Test-NetConnection -ComputerName $hostname -ErrorAction SilentlyContinue -WarningAction SilentlyContinue | select -ExpandProperty PingSucceeded)

        if($hostExists -eq $true)
        {
            foreach($port in $ports){
                echo "Ping to host:$hostname`:$port - STARTED"
                $connectionTestResult = (Test-NetConnection -ComputerName $hostname -Port $port -ErrorAction SilentlyContinue -WarningAction SilentlyContinue)
                $pingSuccessfull = $connectionTestResult | select -ExpandProperty PingSucceeded
                $tcpConnectionSuccessfull = $connectionTestResult | select -ExpandProperty TcpTestSucceeded
                echo "Ping to host:$hostname`:$port - FINISHED - Ping succeeded=$pingSuccessfull TCP Connection succeeded=$tcpConnectionSuccessfull"
            }
        }
        else
        {
            echo "Could not connect to host: $hostname"
        }
    }
}


$RabbitMQPorts = 4369, 5672, 5671, 25672, 15671