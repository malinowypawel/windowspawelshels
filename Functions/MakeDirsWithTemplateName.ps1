function MakeDirsWithTemplateName 
{
    Param([Parameter(Mandatory=$true)][string]$folderNameTemplate,
        [Parameter(Mandatory=$true)][int] $count);
    for($i = 1; $i -le $count; $i++){
        New-Item -Name $folderNameTemplate.Replace("%count%", $i.ToString("00")) -ItemType Directory
    }
}