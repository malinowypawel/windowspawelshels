﻿function CreateFileInAllDirectories
{
    Param([Parameter(Mandatory=$true)][string]$filename,
        [Parameter(Mandatory=$true)][AllowEmptyString()][string] $fileContent,
        [object]$encoding);
    if($encoding -eq $null)
    {
        $encoding = New-Object System.Text.UTF8Encoding;
    }
    Get-ChildItem -Recurse -Path ".\" -Directory | SELECT FullName | ForEach-Object -Process {
        $filePath = $_.FullName + "\" + $filename;
        [System.IO.File]::WriteAllLines($filePath, $fileContent, $encoding);
    }
}