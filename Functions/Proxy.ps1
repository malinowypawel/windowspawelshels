﻿function ProxyStatus 
{
    if ((Get-ItemProperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyEnable | select -ExpandProperty ProxyEnable) -eq 1) 
    { 
        echo "Proxy: Enabled"
    }
    else
    { 
        echo "Proxy: Disabled" 
    }
}
New-Alias ProxyS ProxyStatus
function EnableProxy 
{
    . (Join-Path $PSScriptRoot "Proxy.config.ps1")
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyEnable -value 1
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyServer -value $ProxyServerName
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyOverride -value "<local>"
    ProxyS
}
New-Alias ProxyE EnableProxy
function DisableProxy 
{
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyEnable -value 0
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyServer -value ""
    set-itemproperty 'HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings' -name ProxyOverride -value ""
    ProxyS
}
New-Alias ProxyD DisableProxy

