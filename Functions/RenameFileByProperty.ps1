﻿Function Get-FileMetaData 
{ 
 Param([Parameter(Mandatory=$true)] [string[]]$folder) 
 foreach($sFolder in $folder) 
  { 
   $a = 0 
   $objShell = New-Object -ComObject Shell.Application 
   $objFolder = $objShell.namespace($sFolder) 
 
   foreach ($File in $objFolder.items()) 
    {  
     $FileMetaData = New-Object PSOBJECT 
      for ($a ; $a  -le 266; $a++) 
       {  
         if($objFolder.getDetailsOf($File, $a)) 
           { 
             $hash += @{$($objFolder.getDetailsOf($objFolder.items, $a))  = 
                   $($objFolder.getDetailsOf($File, $a)) } 
            $FileMetaData | Add-Member $hash 
            $hash.clear()  
           }
       } 
     $a=0 
     $FileMetaData 
    } 
  } 
}

Function Rename-FilesInFolder-ByProperty 
{ 
    Param(
        [Parameter(Mandatory=$true)]
        [string]$folder,
        [Parameter(Mandatory=$true)]
        [string] $property) 
    $photos = Get-FileMetaData -folder $folder;
    $unknownName = 0;
    foreach($item in $photos)
    {
        $currentName = $item.Nazwa.ToString();
        $propertyData = "";
        if([bool]($item.PSobject.Properties.name -match $property))
        {
            $propertyData = ($item | select -ExpandProperty $property) -replace ":","_" -replace "\.","_" -replace " ","__"
        }
        else
        {
            $propertyData = "Unknown_$unknownName";
            $unknownName++;
        }
        $newName = $currentName -replace ".*\.","$propertyData."
        $i = 0
        while([System.IO.File]::Exists("$folder\$newName"))
        {
            $newName = $currentName -replace ".*\.","${propertyData}_$i.";
            $i++;
        }
        echo "$folder\$currentName ==> $newName"
        Rename-Item "$folder\$currentName" -NewName "$newName"
    }
}

#Example: Rename-FilesInFolder-ByProperty -folder "C:\Users\Pawel\Desktop\Zdjęcia" -property 'Data wykonania'