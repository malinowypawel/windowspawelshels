$FunctionsDirectory = [Environment]::GetFolderPath("MyDocuments")+"\WindowsPowerShell\Functions"
Write-Host "Loading PawelShell functions to system, from directory:" -ForegroundColor Blue
Write-Host "$FunctionsDirectory" -ForegroundColor Yellow
$powershellScripts = Get-ChildItem "$FunctionsDirectory\*.ps1" 
 foreach($script in $powershellScripts)
 {
    Write-Host "Including: $script" -ForegroundColor Magenta
    .$script;
 }
 Write-Host ''